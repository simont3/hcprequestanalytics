HCP request analytics (|release|)
=================================

..  Tip::

    Make sure to use **version 1.4.0** or better for HCP 8.x logs!

**hcprequestanalytics** reads HTTP access logs from log packages created by
*Hitachi Content Platform* (HCP), loads the content into a SQL database and
runs SQL-queries against it to provide information about topics like:

*   types of requests
*   types of requests to specific HCP nodes
*   types of reuests from specific clients
*   HTTP return codes
*   size distribution of requested objects
*   HCP internal latency distribution
*   clients

It can be easily extended with individual queries (see :doc:`30_queries`).

Results are generated as a multi-sheet XLSX workbook per default; optionally,
CSV files can be requested.


..  toctree::
    :maxdepth: 2

    10_install
    15_syntax
    20_use
    30_queries
    50_interpretation
    80_goodtoknow
    85_info
    97_changelog
    98_license


