Command Syntax
==============

**hcprequestanalytics** consists of several subcommands, each used for a
specific piece of work. Use ``--help`` (or ``-h``) for details:

.. code-block:: text
   :emphasize-lines: 1

    $ hcprequestanalytics -h
    usage: hcprequestanalytics [-h] [--version]
                               {load,analyze,showqueries,dumpqueries} ...

    positional arguments:
      {load,analyze,showqueries,dumpqueries}
        load                load the database
        analyze             analyze the database
        showqueries         show the available queries
        dumpqueries         dump the built-in queries to stdout

    optional arguments:
      -h, --help            show this help message and exit
      --version             show program's version number and exit

load
----

The ``load`` subcommand loads the http gateway logs into a *sqlite3* database
file for later analytics:

.. code-block:: text
   :emphasize-lines: 1

    $ hcprequestanalytics load -h
    usage: hcprequestanalytics load [-h] -d DB logpkg

    positional arguments:
      logpkg      the HCP log package to process

    optional arguments:
      -h, --help  show this help message and exit
      -d DB       the database file

showqueries
-----------

The ``showqueries`` subcommand shows the avaible queries - the ones built-in
as well as the ones added through the ``-a`` parameter:

.. code-block:: text
   :emphasize-lines: 1

    $ hcprequestanalytics showqueries -h
    usage: hcprequestanalytics showqueries [-h] [-a ADDITIONALQUERIES] [-1]

    optional arguments:
      -h, --help            show this help message and exit
      -a ADDITIONALQUERIES  a file containg addition queries (see documentation)
      -1                    print a concatenated list of queries, for easy cut and
                            paste

analyze
-------

The ``analyze`` subcommand runs queries against the database created with the
``load`` subcommand to create an xlsx file as result. Alternatively, a set of
csv files can be requested as well.

.. code-block:: text
   :emphasize-lines: 1

    $ hcprequestanalytics analyze -h
    usage: hcprequestanalytics analyze [-h] [-a ADDITIONALQUERIES] -d DB
                                       [-p PREFIX] [-c] [--procs PROCESSES]
                                       [queries [queries ...]]

    positional arguments:
      queries               a list of query names, or nothing for "all"; you can
                            select a group of queries by using the first few
                            characters followed by an asteriks ('req*' for
                            example)

    optional arguments:
      -h, --help            show this help message and exit
      -a ADDITIONALQUERIES  a file containg addition queries (see documentation)
      -d DB                 the database file
      -p PREFIX             prefix for the output files
      -s                    analyze requests recorded by snodes
      -c                    create CSV files instead of a XLSX file
      --procs PROCESSES     no. of subprocesses to run, defaults to no. of CPUs

dumpqueries
-----------

The ``dumpqueries`` subcommand dumps the built-in queries to stdout. They can
be used as templates to build own queries for use with the ``-a`` parameter:

.. code-block:: text
   :emphasize-lines: 1

    $ hcprequestanalytics dumpqueries -h
    usage: hcprequestanalytics dumpqueries [-h]

    optional arguments:
      -h, --help  show this help message and exit
