Get info
========

You might want to see what's going on, especially if you are running
**hcprequestanalytics** against a huge database.

Queries running
---------------

As the queries are running in parallel, you will receive info about its success
(or fail) once each query has ended. To find out which queries are running at the
moment, you can run this command in a second session::

    $ lsof 2>/dev/null | grep '__\*'
    hcpreques 602  sm  16u  REG  1,5  0  5461618  /private/var/folders/y3/74nllcpj5f511sgw18t55_qh0000gn/T/I_am__*clientip_httpcode*__pbxvbswl
    hcpreques 603  sm  17u  REG  1,5  0  5461730  /private/var/folders/y3/74nllcpj5f511sgw18t55_qh0000gn/T/I_am__*clientip_request_httpcode*__198td_f7

In this example, the string **I_am__*clientip_httpcode*__pbxvbswl**
in the last field of the output indicates that process **602** (the second
field) runs the **clientip_httpcode** query.


Disk space used for tmp indexes
-------------------------------

To find out how much disk space is used for temporary database indexes, you can
run::

    $ lsof 2>/dev/null | grep /wherever/you/have/enough/space
    hcpreques  602  sm  txt  REG  1,2  5301620171   26454 /wherever/you/have/enough/space/etilqs_rrlN0dgfFfwQg9E
    hcpreques  602  sm  18u  REG  1,2  5302781691   26454 /wherever/you/have/enough/space/etilqs_rrlN0dgfFfwQg9E
    hcpreques  603  sm  txt  REG  1,2  1256108032   26456 /wherever/you/have/enough/space/etilqs_7QxuTtMv8AtPYnw
    hcpreques  603  sm  19u  REG  1,2  1256108032   26456 /wherever/you/have/enough/space/etilqs_7QxuTtMv8AtPYnw

You will have to replace ``/wherever/you/have/enough/space`` by the folder
you are using for the temporary database indexes (see :doc:`80_goodtoknow` for
details).

The 7th field will tell you how many bytes are actually used for this single
temporary database index. Be aware that each temporary index shows up twice in
this output, as it is opend twice by the process. The slight difference in size
is caused by the process writing into the index during ``lsof`` was running. The
2nd field will tell you the pid of the process running the query using this
temporary index.

BTW, you will **not** see the files containing the indexes in the filesystem,
and they will not be accounted for when using the ``df`` or ``du`` commands.
