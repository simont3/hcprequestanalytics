Installation
============

Pre-built Executable
--------------------

For most modern *Linux* derivates you should be able to simply run the
executable provided
`here <https://gitlab.com/simont3/hcprequestanalytics/blob/master/src/dist/hcprequestanalytics.linux>`_.

There is also a binary for macOS, built and tested with macOS Sierra (10.12.6)
`here <https://gitlab.com/simont3/hcprequestanalytics/blob/master/src/dist/hcprequestanalytics.macos>`_.

Grab it there, move it to a folder in your $PATH (``/usr/local/bin``, for
example) and follow the instructions in the :doc:`20_use` chapter.


Self-built Executable
---------------------

In case the provided executable fails to run on your system, you can easily
build it on your own. Here's how to do that:

*   Clone the repository from
    `GitLab <https://gitlab.com/simont3/hcprequestanalytics.git>`_::

        $ git clone https://gitlab.com/simont3/hcprequestanalytics.git

*   Change into the project folder and create a Python 3 virtual environment
    and activate it::

        $ cd hcprequestanalytics/src
        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Update pip and setuptools, then load all required dev-packages::

        (.venv) $ pip install -U setuptools pip
        (.venv) $ pip install -r pip-requirements-dev.txt

*   Run the build tool::

        (.venv) $ pyinstaller hcprequestanalytics.spec

    You should find the executable in the ``dist`` subfolder.

    ..  Tip::

        Most likely, in ``hcprequestanalytics.spec``, you will have to adopt
        the path set in the ``_pathext`` variable to your setup!

*   Now move it to a folder in your $PATH (``/usr/local/bin``, for example) and
    follow the instructions in the :doc:`20_use` chapter.


Python virtual environment
--------------------------

*   Create a Python 3 virtual environment and activate it::

        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Install the **hcprequestanalytics** package::

        (.venv) $ pip install -U setuptools pip
        (.venv) $ pip install hcprequestanalytics

*   Now you can run **hcprequestanalytics** this way::

        (.venv) $ hcprequestanalytics

..  Note::

    Remember, every time you want to run **hcprequestanalytics**, you need to
    activate the virtual environment before!


