# The MIT License (MIT)
#
# Copyright (c) 2017-2020 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


[count]
comment : No. of records, overall
query : SELECT count(*) as count FROM logrecs
freeze pane : A5

[count_mqequeries]
comment : No. of records to the MQE, overall
query : SELECT count(*) as count
        FROM logrecs WHERE path GLOB '/query*';
freeze pane : A5

[clientip_mqequeries]
comment : No. of records to the MQE, per client IP address
query : SELECT clientip, count(*) as count
        FROM logrecs where path glob '/query*' GROUP BY clientip;
freeze pane : A5

[clientip]
comment : No. of records per client IP address
query : SELECT clientip, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY clientip
freeze pane : C5

[clientip_httpcode]
comment : No. of records per http code per client IP address
query : SELECT clientip, httpcode, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY clientip, httpcode
freeze pane : D5

[clientip_request_httpcode]
comment : No. of records per http code per request per client IP address
query : SELECT clientip, request, httpcode, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY clientip, request, httpcode
freeze pane : E5

[req]
comment : No. of records per request
query : SELECT request, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY request
freeze pane : C5

[req_httpcode]
comment : No. of records per http code per request
query : SELECT request, httpcode, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY request, httpcode
freeze pane : C5

[req_httpcode_node]
comment : No. of records per node per http code per request
query : SELECT request, httpcode, node, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY request, httpcode, node
freeze pane : E5

[node]
comment : No. of records per node
query : SELECT node, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY node
freeze pane : C5

[clientip_node]
comment : No. of records per clientip per node
query : SELECT clientip, node, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY clientip, node
freeze pane : D5

[node_req]
comment : No. of records per request per node
query : SELECT node, request, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY node, request
freeze pane : D5

[node_req_httpcode]
comment : No. of records per http code per request per node
query : SELECT node, request, httpcode, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY node, request, httpcode
freeze pane : E5

[day]
comment : No. of records per day
query : SELECT printf("%%s/%%s", substr(timestampstr, 4, 3),
                               substr(timestampstr, 1, 2)) AS day,
               count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY day
freeze pane : C5

[day_req]
comment : No. of records per request per day
query : SELECT printf("%%s/%%s", substr(timestampstr, 4, 3),
                               substr(timestampstr, 1, 2)) AS day,
               request, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY day, request
freeze pane : D5

[day_hour]
comment : No. of records per hour per day
query : SELECT printf("%%s/%%s", substr(timestampstr, 4, 3),
                               substr(timestampstr, 1, 2)) AS day,
               printf("%%s", substr(timestampstr, 13, 2)) AS hour,
               count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY day, hour
freeze pane : D5

[day_hour_req]
comment : No. of records per request per hour per day
query : SELECT printf("%%s/%%s", substr(timestampstr, 4, 3),
                               substr(timestampstr, 1, 2)) AS day,
               printf("%%s", substr(timestampstr, 13, 2)) AS hour,
               request, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY day, hour, request
freeze pane : E5

[day_req_httpcode]
comment : No. of records per http code per request per day
query : SELECT printf("%%s/%%s", substr(timestampstr, 4, 3),
                               substr(timestampstr, 1, 2)) AS day,
               request, httpcode, count(*) as count,
        min(size), avg(size), max(size),
        min(latency), avg(latency),
        max(latency)
        FROM logrecs GROUP BY day, request, httpcode
freeze pane : E5

[500_largest_size]
comment : The records with the 500 largest requests sorted by size
query : SELECT request, httpcode, node, latency, size,
        tp(size,latency) as Bytes_sec, clientip, user,
        timestamp, timestampstr, path, namespace
        FROM logrecs ORDER BY size DESC LIMIT 500
freeze pane : D5

[500_largest_req_httpcode_node]
comment : The records with the 500 largest requests by req, httpcode, node
query : SELECT request, httpcode, node, latency, size,
        tp(size,latency) as Bytes_sec, clientip, user,
        timestamp, timestampstr, path, namespace
        FROM (SELECT * FROM logrecs ORDER BY size DESC LIMIT 500)
        ORDER BY request, httpcode, node
freeze pane : D5

[500_worst_latency]
comment : The records with the 500 worst latencies
query : SELECT request, httpcode, latency, size, tp(size,latency) as Bytes_sec,
               clientip, user, timestamp, timestampstr, path, namespace
            FROM (SELECT * FROM logrecs WHERE size > 0
                      ORDER BY latency DESC LIMIT 500)
            ORDER BY request, httpcode
freeze pane : C5

[percentile_req]
comment : No. of records per request analysis, including percentiles for size and latency
query : SELECT request, count(*) as count,
        min(size), avg(size), max(size),
        percentile(size, 10) as 'pctl-10 (size)',
        percentile(size, 20) as 'pctl-20 (size)',
        percentile(size, 30) as 'pctl-30 (size)',
        percentile(size, 40) as 'pctl-40 (size)',
        percentile(size, 50) as 'pctl-50 (size)',
        percentile(size, 60) as 'pctl-60 (size)',
        percentile(size, 70) as 'pctl-70 (size)',
        percentile(size, 80) as 'pctl-80 (size)',
        percentile(size, 90) as 'pctl-90 (size)',
        percentile(size, 95) as 'pctl-95 (size)',
        percentile(size, 99) as 'pctl-99 (size)',
        percentile(size, 99.9) as 'pctl-99.9 (size)',
        min(latency), avg(latency),
        max(latency),
        percentile(latency, 10) as 'pctl-10 (latency)',
        percentile(latency, 20) as 'pctl-20 (latency)',
        percentile(latency, 30) as 'pctl-30 (latency)',
        percentile(latency, 40) as 'pctl-40 (latency)',
        percentile(latency, 50) as 'pctl-50 (latency)',
        percentile(latency, 60) as 'pctl-60 (latency)',
        percentile(latency, 70) as 'pctl-70 (latency)',
        percentile(latency, 80) as 'pctl-80 (latency)',
        percentile(latency, 90) as 'pctl-90 (latency)',
        percentile(latency, 95) as 'pctl-95 (latency)',
        percentile(latency, 99) as 'pctl-99 (latency)',
        percentile(latency, 99.9) as 'pctl-99.9 (latency)'
        FROM logrecs GROUP BY request
freeze pane : C5

[500_highest_throughput]
comment : The 500 records with the highest throughput (Bytes/sec)
query : SELECT request, httpcode, clientip, tp(size, latency) as Bytes_sec,
               latency, size, user, timestamp, timestampstr, path, namespace
             FROM logrecs
             WHERE size > 0 and latency > 0
             ORDER BY Bytes_sec DESC LIMIT 500;
freeze pane : D5

[percentile_throughput_128kb]
comment : No. of records per request, with percentiles on throughput (Bytes/sec) for objects >= 128KB
query : SELECT request, count(*) as count,
        min(tp(size, latency)) as 'min(B/sec)',
        avg(tp(size, latency)) as 'avg(B/sec)',
        max(tp(size, latency)) as 'max(B/sec)',
        percentile(tp(size, latency), 10) as 'pctl-10 (B/sec)',
        percentile(tp(size, latency), 20) as 'pctl-20 (B/sec)',
        percentile(tp(size, latency), 30) as 'pctl-30 (B/sec)',
        percentile(tp(size, latency), 40) as 'pctl-40 (B/sec)',
        percentile(tp(size, latency), 50) as 'pctl-50 (B/sec)',
        percentile(tp(size, latency), 60) as 'pctl-60 (B/sec)',
        percentile(tp(size, latency), 70) as 'pctl-70 (B/sec)',
        percentile(tp(size, latency), 80) as 'pctl-80 (B/sec)',
        percentile(tp(size, latency), 90) as 'pctl-90 (B/sec)',
        percentile(tp(size, latency), 95) as 'pctl-95 (B/sec)',
        percentile(tp(size, latency), 99) as 'pctl-99 (B/sec)',
        percentile(tp(size, latency), 99.9) as 'pctl-99.9 (B/sec)'
        FROM logrecs where size >= 131072 GROUP BY request
freeze pane : C5

[ten_ns_proto_httpcode]
comment : No. of records per Tenant / Namespace / protocol / http code
query : SELECT getTenant(namespace) as Tenant,
               getNamespace(path, namespace) as Namespace,
               getProtocol(namespace) as Protocol, httpcode,
               count(*) as count
        FROM logrecs GROUP BY getTenant(namespace),
                              getNamespace(path, namespace),
                              getProtocol(namespace), httpcode
freeze pane : D5

[ten_ns_proto_percentile_req]
comment : No. of records per Tenant / Namespace / protocol, including percentiles for size and latency
query : SELECT getTenant(namespace) as Tenant,
               getNamespace(path, namespace) as Namespace,
               getProtocol(namespace) as Protocol, request, count(*) as count,
        min(size), avg(size), max(size),
        percentile(size, 10) as 'pctl-10 (size)',
        percentile(size, 20) as 'pctl-20 (size)',
        percentile(size, 30) as 'pctl-30 (size)',
        percentile(size, 40) as 'pctl-40 (size)',
        percentile(size, 50) as 'pctl-50 (size)',
        percentile(size, 60) as 'pctl-60 (size)',
        percentile(size, 70) as 'pctl-70 (size)',
        percentile(size, 80) as 'pctl-80 (size)',
        percentile(size, 90) as 'pctl-90 (size)',
        percentile(size, 95) as 'pctl-95 (size)',
        percentile(size, 99) as 'pctl-99 (size)',
        percentile(size, 99.9) as 'pctl-99.9 (size)',
        min(latency), avg(latency),
        max(latency),
        percentile(latency, 10) as 'pctl-10 (latency)',
        percentile(latency, 20) as 'pctl-20 (latency)',
        percentile(latency, 30) as 'pctl-30 (latency)',
        percentile(latency, 40) as 'pctl-40 (latency)',
        percentile(latency, 50) as 'pctl-50 (latency)',
        percentile(latency, 60) as 'pctl-60 (latency)',
        percentile(latency, 70) as 'pctl-70 (latency)',
        percentile(latency, 80) as 'pctl-80 (latency)',
        percentile(latency, 90) as 'pctl-90 (latency)',
        percentile(latency, 95) as 'pctl-95 (latency)',
        percentile(latency, 99) as 'pctl-99 (latency)',
        percentile(latency, 99.9) as 'pctl-99.9 (latency)'
        FROM logrecs GROUP BY getTenant(namespace),
                              getNamespace(path, namespace),
                              getProtocol(namespace), request
freeze pane : E5

[ten_ns_proto_user_httpcode]
comment : No. of records per Tenant / Namespace / protocol / user / http code
query : SELECT getTenant(namespace) as Tenant,
               getNamespace(path, namespace) as Namespace,
               getProtocol(namespace) as Protocol, user, httpcode,
               count(*) as count
        FROM logrecs GROUP BY getTenant(namespace),
                              getNamespace(path, namespace),
                              getProtocol(namespace), user, httpcode
freeze pane : E5

[ten_ns_proto_clientip_httpcode]
comment : No. of records per Tenant / Namespace / protocol / client IP address / http code
query : SELECT getTenant(namespace) as Tenant,
               getNamespace(path, namespace) as Namespace,
               getProtocol(namespace) as Protocol, clientip, httpcode,
               count(*) as count
        FROM logrecs GROUP BY getTenant(namespace),
                              getNamespace(path, namespace),
                              getProtocol(namespace), clientip, httpcode
freeze pane : E5

[ten_proto_httpcode]
comment : No. of records per Tenant / protocol / http code
query : SELECT getTenant(namespace) as Tenant,
               getProtocol(namespace) as Protocol, httpcode,
               count(*) as count
        FROM logrecs GROUP BY getTenant(namespace),
                              getProtocol(namespace), httpcode
freeze pane : C5

[500_httpcode_409]
comment : The 500 newest records with http code 409
query : SELECT *
        FROM logrecs WHERE httpcode = '409' ORDER BY timestamp DESC LIMIT 500
freeze pane : A5

[500_httpcode_413]
comment : The 500 newest records with http code 413
query : SELECT *
        FROM logrecs WHERE httpcode = '413' ORDER BY timestamp DESC LIMIT 500
freeze pane : A5

[500_httpcode_503]
comment : The 500 newest records with http code 503
query : SELECT *
        FROM logrecs WHERE httpcode = '503' ORDER BY timestamp DESC LIMIT 500
freeze pane : A5

[ten_user_ns_req_http]
comment : Tenants with all users accessing Namespaces, incl. request and httpcode
query : SELECT getTenant(namespace) as Tenant, user,
               getNamespace(path, namespace) as Namespace,
               request, httpcode, count(*) as count
        FROM logrecs GROUP BY getTenant(namespace), user,
                              getNamespace(path, namespace),
                              request, httpcode
Freeze pane : C5

[mapi_endp_req_http]
comment : MAPI request: endpoints, request, http code
query : SELECT getMapiPath(path) as endpoint, request, httpcode,
               count(*) as count,
               min(latency), avg(latency), max(latency)
        FROM mapirecs GROUP BY getMapiPath(path), request, node, httpcode
Freeze pane : A5

[mapi_user_req_http]
comment : MAPI requests by user
query : SELECT user, getMapiPath(path) as endpoint, request, node, httpcode,
               count(*) as count,
               min(latency), avg(latency), max(latency)
        FROM mapirecs GROUP BY user, getMapiPath(path), request, node, httpcode
Freeze pane : A5

